package test.alexander.daggerinjecttest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import test.alexander.daggerinjecttest.api.SomeApi;

public class MainActivity extends Activity {
    @Inject
    SomeApi mSomeApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tvText = (TextView) findViewById(R.id.tvText);
        Button btn = (Button) findViewById(R.id.btn);

        tvText.setText(mSomeApi.toString());
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MainActivity2.class));
            }
        });
    }
}
