package test.alexander.daggerinjecttest.di.scope;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by ALEXANDER on 17.03.20167
 */
@Scope
@Retention(RUNTIME)
public @interface FragmentScope {
}