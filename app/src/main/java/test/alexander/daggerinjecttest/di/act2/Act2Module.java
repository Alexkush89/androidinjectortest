package test.alexander.daggerinjecttest.di.act2;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import test.alexander.daggerinjecttest.MainActivity2;

/**
 * Created by Alexander Kushnarev on 01.06.2017.
 * akushnarev@ronte.io
 */
@Module
public abstract class Act2Module {
    @Binds
    abstract Activity provideActivity(MainActivity2 activity);
}
