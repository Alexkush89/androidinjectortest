package test.alexander.daggerinjecttest.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import test.alexander.daggerinjecttest.MainActivity;
import test.alexander.daggerinjecttest.MainActivity2;
import test.alexander.daggerinjecttest.TestFragment;
import test.alexander.daggerinjecttest.di.act2.Act2Module;
import test.alexander.daggerinjecttest.di.scope.ActivityScope;
import test.alexander.daggerinjecttest.di.scope.FragmentScope;

/**
 * Created by Alexander Kushnarev on 01.06.2017.
 * akushnarev@ronte.io
 */
@Module
abstract class AndroidBindingModule {
    @ActivityScope
    @ContributesAndroidInjector
    abstract MainActivity mainActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {Act2Module.class})
    abstract MainActivity2 mainActivity2();

    @FragmentScope
    @ContributesAndroidInjector
    abstract TestFragment testFragment();
}
