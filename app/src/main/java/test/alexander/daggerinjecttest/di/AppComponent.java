package test.alexander.daggerinjecttest.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import test.alexander.daggerinjecttest.TheApp;

/**
 * Created by Alexander Kushnarev on 01.06.2017.
 * akushnarev@ronte.io
 */
@Singleton
@Component(
        modules = {
                AndroidInjectionModule.class,
                AndroidBindingModule.class,
                AppModule.class
        }
)
interface AppComponent extends AndroidInjector<TheApp> {
    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<TheApp> {
        @BindsInstance
        public abstract Builder application(Application application);
    }
}
