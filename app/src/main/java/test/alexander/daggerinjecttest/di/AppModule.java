package test.alexander.daggerinjecttest.di;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ALEXANDER on 01.06.2017.
 * akushnarev@ronte.io
 */
@Module
class AppModule {
    @Provides
    Context provideContext(Application application) {
        return application.getApplicationContext();
    }
}
