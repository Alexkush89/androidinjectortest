package test.alexander.daggerinjecttest;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import test.alexander.daggerinjecttest.di.DaggerAppComponent;

/**
 * Created by Alexander Kushnarev on 01.06.2017.
 * akushnarev@ronte.io
 */
public class TheApp extends DaggerApplication {
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder()
                .application(this)
                .create(this);
    }
}
