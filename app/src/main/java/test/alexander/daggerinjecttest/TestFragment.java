package test.alexander.daggerinjecttest;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import test.alexander.daggerinjecttest.api.SomeApi;

/**
 * Created by Alexander Kushnarev on 06.06.2017.
 * akushnarev@ronte.io
 */
public class TestFragment extends Fragment {
    @Inject
    SomeApi mSomeApi;

    @Override
    public void onAttach(Context context) {
        AndroidInjection.inject(this);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_test1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        TextView tvText = (TextView) view.findViewById(R.id.tvText);
        tvText.setText(mSomeApi.toString());
    }
}
