package test.alexander.daggerinjecttest.api;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Alexander Kushnarev on 01.06.2017.
 * akushnarev@ronte.io
 */
@Singleton
public class SomeApi {
    private final Context mContext;

    @Inject
    SomeApi(Context context) {
        mContext = context;
    }

    @Override
    public String toString() {
        return super.toString() + "SomeApi{" +
                "mContext=" + mContext +
                '}';
    }
}
