package test.alexander.daggerinjecttest.api;

import android.app.Activity;

import javax.inject.Inject;

import test.alexander.daggerinjecttest.di.scope.ActivityScope;

/**
 * Created by Alexander Kushnarev on 01.06.2017.
 * akushnarev@ronte.io
 */
@ActivityScope
public class ApiWithActivity {
    private Activity mActivity;

    @Inject
    ApiWithActivity(Activity activity) {
        mActivity = activity;
    }
}
