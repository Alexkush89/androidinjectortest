package test.alexander.daggerinjecttest;

import android.os.Bundle;
import android.widget.TextView;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.DaggerActivity;
import test.alexander.daggerinjecttest.api.ApiWithActivity;
import test.alexander.daggerinjecttest.api.SomeApi;

public class MainActivity2 extends DaggerActivity {
    @Inject
    SomeApi mSomeApi;
    @Inject
    ApiWithActivity mApiWithActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        TextView tvText = (TextView) findViewById(R.id.tvText);
        tvText.setText(mSomeApi.toString());

        getFragmentManager()
                .beginTransaction().add(R.id.vgContainer, new TestFragment())
                .commit();
    }
}
